#!/bin/bash

oc delete secret minio-auth

oc create secret generic minio-auth \
    --from-literal=MINIO_ACCESS_KEY="$(vault read -field=MINIO_ACCESS_KEY secret/gitlab/ci/cache/minio-systems-ci.cloud.duke.edu)" \
    --from-literal=MINIO_SECRET_KEY="$(vault read -field=MINIO_SECRET_KEY secret/gitlab/ci/cache/minio-systems-ci.cloud.duke.edu)"
