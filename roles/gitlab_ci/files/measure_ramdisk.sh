#!/usr/bin/env bash

MOUNT=/srv/docker-machines

RAMDISK_USED=$(df ${MOUNT} | tail -1 | awk '{print $5}' | sed -e 's/%//g')

echo "docker_machine_ramdisk,mount=${MOUNT} used_percent=${RAMDISK_USED} $(date +%s)"
