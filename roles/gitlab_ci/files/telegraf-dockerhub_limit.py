#!/usr/bin/env python3
import sys
import argparse
import json
import requests
import logging
import subprocess
from datetime import datetime


def parse_args():
    """Parse the arguments."""
    parser = argparse.ArgumentParser(
        description="Check dockerhub limits",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    parser.add_argument(
        "-v", "--verbose", help="Be verbose", action="store_true", dest="verbose"
    )

    return parser.parse_args()


def main():
    args = parse_args()
    if args.verbose:
        logging.basicConfig(format="%(levelname)s: %(message)s", level=logging.DEBUG)
    else:
        logging.basicConfig(format="%(levelname)s: %(message)s", level=logging.INFO)

    params = {
        "service": "registry.docker.io",
        "scope": "repository:ratelimitpreview/test:pull",
    }
    token_request = requests.get(
        "https://auth.docker.io/token",
        params=params,
    )
    token = token_request.json()["token"]
    headers = {"Authorization": "Bearer %s" % token}
    limit_request = requests.head(
        "https://registry-1.docker.io/v2/ratelimitpreview/test/manifests/latest",
        headers=headers,
        proxies={"https": "https://proxy.oit.duke.edu:3128"}
    )
    limit_raw = limit_request.headers["RateLimit-Limit"]
    current_raw = limit_request.headers["RateLimit-Remaining"]
    limit, limit_window_raw = limit_raw.split(";")
    current, current_window_raw = current_raw.split(";")
    limit_window = limit_window_raw.split("=")[1]
    current_window = current_window_raw.split("=")[1]
    print(
        "dockerhub_limit,user=%s limit=%s,limit_window=%s,current=%s,current_window=%s %s"
        % (
            "anonymous",
            limit,
            limit_window,
            current,
            current_window,
            datetime.now().strftime("%s"),
        )
    )
    return 0


if __name__ == "__main__":
    sys.exit(main())
